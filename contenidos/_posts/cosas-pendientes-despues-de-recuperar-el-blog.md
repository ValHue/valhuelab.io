---
title: "Cosas pendientes después de haber recuperado el Blog."
categories: ["El Blog"]
date: "2013-08-23T12:14:36+01:00"
tags: ["El Blog", "Web"]
---

### &nbsp; ###

## ¡Por fin!.

Ya he "rescatado" todas las entradas del blog, cuando este funcionaba sobre WordPress, he ajustado el tema que utilizo, algunas cosillas más, pero el trabajo no está terminado. Aún tengo pensado hacer:

*   *Dotar de un* ***motor de búsqueda*** *interno al Blog*.
*   *Personalizar las páginas de errores*.
*   *Implementar un sistema de* ***smiles***, que sustituya el equivalente en texto por un gráfico.
*   *Usar un sistema de seguridad que evite el borrado o editado accidental de entradas*.
*   *Crear una* ***página de archivos***.
*   *Mejorar la* ***página de descargas***, *que no solo muestre aquellas entradas con la categoría*.<!-- more -->

De momento creo que es bastante trabajo pendiente, pero aún así se aceptan ideas, enviar vuestros comentarios.

&nbsp; :D &nbsp;

***

### PD: ###

Una lástima haber perdido los comentarios. No he podido rescatarlos de WordPress e incorporarlos a Disqus.

&nbsp; :( &nbsp;

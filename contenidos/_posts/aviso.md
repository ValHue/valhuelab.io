---
title: AVISO
date: 2015/12/22 12:49:20
tags: [el blog, programación web, themes, hugo]
categories: [El Blog]
---


Como habéis podido comprobar estoy cambiando el blog.

- He dejado atrás Pelican y me he cambiado a Hugo.
- Estoy realizando los ajustes necesarios para obtener el aspecto y la funcionalidad que deseo. No sé cuanto tiempo me llevará esto, por lo que os pido paciencia.

PD: Últimamente tenía un poco olvidado el blog, y otras muchas cosas, pero la verdad era que no disponía de tiempo libre para ello. Os pido disculpas por ello.

&nbsp; ;) &nbsp;

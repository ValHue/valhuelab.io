---
title: "Definitivamente me quedo con ArchLinux"
date: "2012-11-30T10:55:22+01:00"
categories: ["Linux"]
tags: ["ArchLinux", "Linux"]
---

Pues como bien dice el titulo, después de pasar por muchas distribuciones, me quedo con [ArchLinux][1].

[![][2]][3]

 ¿Los motivos?, ¡Muchos!<!-- more -->

Incluso existe alguna web donde dan [mil razones para abandonar Ubuntu y Usar ArchLinux][4], no voy a crear ninguna entrada criticando a ninguna distribución, porque para mí lo importante es que linux vaya llegando al usuario final. Motivo por el cual he alojado en este blog un antiguo proyecto que invitaba a probar linux, [obtenga Linux][5], un proyecto lamentablemente abandonado de la organización GNU/Linux Matters.

Quizás os estáis preguntando que hace tan diferente a [**Arch**][6] de otras distros, bueno [**aquí**][7] tenéis una comparación. Pero para mi lo fundamental es:

-  [Rolling Release][8].
-  Filosofía [KISS][9].
-  Utiliza [systemd][10].
-  Actualizaciones inmediatas.

Si sientes la tentación de probar ArchLinux, creo que no me equivoco al decir que los mejores tutoriales están en la web de [Gregorio Espadas][11].

La URL de la Comunidad Hispana de ArchLinux: <http://portada.archlinux-es.org/>

Y lo más importante, ¿buscas información?, posiblemente [***la mejor Wiki de una distribución Linux es la de Arch***][12].

Si todavía tienes alguna duda o pregunta, seguramente ya esté respondida en esta [FAQ de ArchLinux][13].

Y si alguien pensaba que he estado perdiendo el tiempo durante este periodo de inactividad, nada mas lejos de la realidad. A parte de migrar a Arch, de configurar debidamente todo mi hardware, he creado algunos paquetes en el repositorio AUR para completar la instalación que perseguía, por si os puede ser útil ahí va la URL: <https://aur.archlinux.org/packages/?SeB=m&K=ValHue>

Saludos.

[1]: http://es.wikipedia.org/wiki/Archlinux "ArchLinux"
[2]: https://valhue.gitlab.io/imagenes/2012/11/ArchlinuxLogo.png "ArchLinux"
[3]: https://www.archlinux.org/ "ArchLinux"
[4]: http://www.identi.li/index.php?topic=34213 "Mil razones para abandonar Ubuntu y usar ArchLinux"
[5]: http://www.getgnulinux.org/ "¡Cámbiate a Linux!"
[6]: https://wiki.archlinux.org/index.php/Arch_Linux_(Espa%C3%B1ol) "Arch Linux (Español)"
[7]: https://wiki.archlinux.org/index.php/Arch_Compared_to_Other_Distributions_(Espa%C3%B1ol) "Arch comparada con otras distribuciones (Español)"
[8]: http://usemoslinux.blogspot.com/2012/06/las-mejores-distribuciones-rolling.html "Rolling Release"
[9]: https://wiki.archlinux.org/index.php/The_Arch_Way_(Espa%C3%B1ol) "KISS"
[10]: https://wiki.archlinux.org/index.php/Systemd_(Espa%C3%B1ol) "Systemd"
[11]: http://gespadas.com/archlinux "Blog de Gregorio Espadas"
[12]: https://wiki.archlinux.org "ArchWiki"
[13]: https://wiki.archlinux.org/index.php/FAQ_(Espa%C3%B1ol) "FAQ de ArchLinux"

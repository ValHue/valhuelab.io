---
title: Instalación y configuración de Arduino en ArchLinux
date: 2016-04-10 21:28:57
categories: Arduino
tags:
  - Arduino
  - Linux
  - ArchLinux
---

{% blockquote %}
Inicio con esta entrada una nueva categoría en el blog dónde iré subiendo distintos artículos orientados a ayudar a aquellas personas interesadas en trabajar con Arduino desde ArchLinux. Empezaremos con lo más básico, {% raw %}***¿cómo se instala en nuestro querido sistema operativo?***{% endraw %}
{% endblockquote %}

<div>&nbsp;</div>

#### **1. ¿Que és Arduino?**

*Arduino es una plataforma electrónica de código abierto basada en hardware y software fáciles de usar. [Las placas Arduino](https://www.arduino.cc/en/Main/Products) pueden leer entradas (luz en un sensor, un dedo en un botón o un mensaje de Twitter) y convertirla en una salida: activar un motor, encender un LED y publicar algo en línea. Puede decirle a su tarjeta qué debe hacer enviando un conjunto de instrucciones al microcontrolador de la tarjeta. Para hacerlo, utiliza el [lenguaje de programación Arduino](https://www.arduino.cc/en/Reference/HomePage) (basado en [Wiring](http://wiring.org.co/) ) y [el software Arduino (IDE)](https://www.arduino.cc/en/Main/Software), basado en el [procesamiento](https://processing.org/)*.

El hardware consiste en una placa electrónica [PCB](https://www.mcielectronics.cl/page/fabricacion-pcbs), de hardware libre, que incorpora un microcontrolador re-programable y una serie <!-- more -->de pines hembra, los que permiten establecer conexiones entre el microcontrolador y los diferentes sensores y actuadores de una manera muy sencilla (principalmente con cables dupont).

![](/imagenes/2016/04/arduino_uno.png)

Al hablar de “Arduino” deberemos especificar el modelo concreto, ya que se han fabricado diferentes modelos de placas Arduino oficiales, cada una pensada con un propósito diferente y características variadas (como el tamaño físico, número de pines E/S, microcontrolador, etc). A partir de ahora en todo lo referente a Arduino en este blog estaré haciendo referencia al Arduino UNO que es la placa de la que dispongo.

{% colorquote info %}
A pesar de las distintas placas que existen todas pertenecen a la misma familia (microcontroladores AVR, ATmega328, de la marca Atmel), esto significa que comparten la mayoría de sus características de software, como arquitectura, librerías y documentación.
{% endcolorquote %}

<div>&nbsp;</div>

#### **2. Instalando el software**

Para instalar el software de arduino en ArchLinux basta con ejecutar desde una consola o terminal:

```bash
sudo pacman -S arduino
```

y si queremos disponer de documentación offline procederemos con:

```bash
sudo pacman -S arduino-docs
```

Después de instalar los paquetes necesarios tenemos que concederle permiso a nuestro usuario a los grupos uucp y lock

```bash
sudo gpasswd -a USUARIO uucp
sudo gpasswd -a USUARIO lock
```
Ahora será necesario reiniciar nuestro sistema para que el usuario tenga los permisos necesarios y podamos enviar a nuestra placa de Arduino el software que desarrollemos. Si aún después de seguir estos pasos tuviésemos problemas para enviar el código a nuestra placa, aconsejo visitar la wiki de ArchLinux, [allí](https://wiki.archlinux.org/index.php/Arduino#Accessing_serial) encontraréis una sección a cómo resolver los problemas de comunicación serie con el dispositivo.

<div>&nbsp;</div>

#### **3. Comprobamos que todo funciona correctamente**

Vamos a cargar un programa de los que el propio software lleva incorporado en los ejemplos.

![](/imagenes/2016/04/arduino_blink.png)

Primero montamos el circuito de la figura, conectando un diodo LED tal y como se muestra. A continuación navegamos por el menú de la aplicación y elegimos uno de los ejemplos de software o programa que lleva en su distribución. Haremos clic en ***Archivo --> Ejemplos --> 01.Basics --> Blink***

![](/imagenes/2016/04/arduino_blink_código.png)

Con esto habremos cargado el código de nuestro micro-programa en la interface de programación de nuestra placa. **Todavía no está subido y guardado** en el micro-controlador de ésta.

Para subirlo y grabarlo en el micro es tan simple cómo hacer clic en el "botón" siguiente:

![](/imagenes/2016/04/arduino_subir_código.png)

Al finalizar la operación observaremos el siguiente mensaje:

![](/imagenes/2016/04/arduino_código_subido.png)

Y podremos observar como el LED de la placa Arduino parpadea, manteniéndose encendido 1000ms y apagado otros 1000ms que es lo que trae programado por defecto. Podemos decir con total rotundidad que nuestro **arduino (software)** está correctamente instalado y funciona.

Que lo disfrutéis ** ;) **
---
title: "¿Te crece .xsession-errors?"
date: "2011-07-04T10:15:11+01:00"
categories: ["Linux"]
tags: ["Natty", "Ubuntu"]
---

#### &nbsp; ####

Anoche me quedo sin espacio en el disco, 0 bytes, y todos los programas que tenia abiertos se me quedaban fritos, o perdía sus configuraciones.

-- ¿Que xxxxx me estaba pasando?

Por la mañana tenia unos 50GB libres, algo se estaba comiendo mi espacio libre, o sea, estaba creciendo y engordando en demasía. Tenía que buscar el fichero que había engordado 50GB y me había dejado sin espacio, arruinando las configuraciones de mi sesión.

Recordé un [post del blog "**Seamos realistas**"][1] donde mencionaban como localizar los archivos más gordos de tu sistema, así que ejecuté:
``` bash
    du -h * | sort -n | tail
```
<!-- more -->y me encuentro con que el archivo de registro de Xsession, el ~/.xsession-errors pesaba 50GB.

Evidentemente esto me estaba dejando sin espacio, pero... ¿tantos errores tengo?. A mi, el sistema (Ubuntu 11.04) instalado, me está funcionando perfectamente, por lo que he decidido desactivar este archivo de registro, y en el caso de necesitarlo para depurar alguna aplicación, pues lo volvería a activar.

Para hacerlo, utilicé [**esta entrada**][2].

Espero que os sirva, y si es un bug de Ubuntu Natty, pues que lo solucionen cuanto antes.

** ;) **

 

[1]: http://fausto23.wordpress.com/2010/03/30/tip-encuentra-los-archivos-que-consumen-mas-espacio/ "(Tip) Encuentra los archivos que consumen más espacio"
[2]: http://blog.dhampir.no/content/moving-or-removing-xsession-errors "Moving or removing ~/.xsession-errors"

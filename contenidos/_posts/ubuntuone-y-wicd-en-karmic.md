---
title: "UbuntuOne y Wicd en karmic"
date: "2009-11-13T11:19:01+01:00"
categories: ["Linux"]
tags: ["Linux", "Karmic", "Ubuntu"]
---

![][3]

Existe una [***dependencia de UbuntuOne***][1], que no permite registrar nuestro PC sin tener instalado NetworkManager.

Pero parece ser que solo necesita de dicho gestor de redes para el registro, porque después de leer el comentario de [***René***][2], he probado a:

-  instalar NetworkManager.
-  registrar mi PC en UbuntuOne.
-  instalar Wicd (mi gestor preferido) y aceptar la des-instalación de NetworkManager.
-  reiniciar....

et voilà!

UbuntuOne, una vez registrado tu PC, no depende de NetworkManager y **¡funciona!**.

[1]: https://bugs.launchpad.net/ubuntuone-client/+bug/357395 "dependencia de UbuntuOne"
[2]: https://bugs.launchpad.net/ubuntuone-client/+bug/357395/comments/39 "comentario de René"
[3]: http://valhue.gitlab.io/banners/ubuntu_karmic.jpg

---
title: "No les votes"
date: "2011-02-15T11:50:12+01:00"
categories: ["General"]
tags: ["Elecciones", "iNet", "Internet", "Sinde", "Telecomunicaciones"]
---

#### &nbsp; ####

![][12]

El próximo 22 de mayo, [***los ciudadanos españoles están convocados a las urnas***][1] para votar a sus representantes públicos en todos los ayuntamientos y en algunos parlamentos autonómicos. Los representantes elegidos tendrán a su cargo la gestión de miles de millones de euros durante un periodo de cuatro años, razón más que suficiente para extremar las precauciones de los votantes: a lo largo de los últimos años, [***el nivel de corrupción en la política española se ha disparado de manera alarmante en todo el arco parlamentario***][2].<!-- more -->

[***PSOE, PP y CiU son las tres formaciones políticas que han pactado para resucitar la ley Sinde en el Senado***][3], una ley que permite censurar Internet por vía administrativa, [***sin una intervención judicial que garantice la tutela efectiva de los ciudadanos***][4]. Al juez que deba validar el cierre le estará vedado analizar el fondo del asunto, esto es, la vulneración de derechos de propiedad intelectual o la posibilidad de producir un perjuicio patrimonial por parte de la página web cuya clausura se solicite. La ley Sinde crea un "agujero libre de jueces" donde la decisión la toma una comisión administrativa nombrada por el gobierno, para evitar lo que hasta el momento venía ocurriendo: que los jueces [***no daban la razón a las reclamaciones***][5] de la industria de los contenidos.

La ley Sinde es [***ineficaz***][6]. No aborda una reforma integral de la legislación de propiedad intelectual, único camino para favorecer la justa retribución de los creadores y artistas en el marco de una [***sociedad de cultura digital***][7]. Aún así, y a pesar de la oposición de una parte importante de la sociedad incluyendo [***creadores y artistas***][8], [***PSOE, PP y CiU votaron a favor de ella***][9]. Pesaron más las [***presiones de gobiernos extranjeros***][10] y de grupos minoritarios que el interés social. Pero no todo es culpa de nuestros representantes: nosotros les hemos elegido, por acción u omisión.

Desde [***Nolesvotes.com***][11] consideramos que PSOE, PP y CiU han faltado a su principal obligación con la ciudadanía: defender la Constitución que juraron o prometieron acatar. La ley Sinde somete Internet a una legislación excepcional, con grave merma de los derechos a la libertad de expresión e información y a la tutela judicial efectiva, posibilitando un mayor control político de la red.

Tu decisión es importante. No te pedimos el voto para ningún partido concreto, ni que votes en blanco, ni que te abstengas, sino que te informes para comprobar que existen alternativas contrarias a la ley Sinde en todo el espectro ideológico. Te pedimos que defiendas la libertad en la red con tu voto, no apoyando a aquellos que con sus actos se han hecho claramente merecedores de un voto de castigo.

El próximo 22 de mayo, **NO LES VOTES**.

(Texto reproducido de [**nolesvotes.com**][11]. Vale la pena pasarse a ver la evolución del contador)

[1]: http://es.wikipedia.org/wiki/Elecciones_municipales_espa%C3%B1olas_de_2011 "Elecciones municipales de España de 2011"
[2]: http://es.wikipedia.org/wiki/Corrupci%C3%B3n_en_Espa%C3%B1a#Gobierno_de_Jos.C3.A9_Luis_Rodr.C3.ADguez_Zapatero "Corrupción en España"
[3]: http://www.elmundo.es/elmundo/2011/01/24/navegante/1295895670.html "PP, PSOE y CiU pactan la resurrección de la Ley Sinde con pocos cambios en el Senado"
[4]: http://www.filmica.com/david_bravo/archivos/010529.html "Carta enviada al Consejo General del Poder Judicial a propósito de la "Ley Sinde""
[5]: http://derecho-internet.org/svn/procedimientos-libres/defensa-webs-enlaces/resoluciones/formato-txt/ "Resoluciones"
[6]: http://andresdelaoliva.blogspot.com/2011/01/la-reaparicion-de-la-ley-sinde-una.html "¿ES QUE NO HAY NADIE EN EL PARLAMENTO ESPAÑOL QUE SEPA DERECHO?"
[7]: http://www.youtube.com/watch?v=Z2PmCEPE5iI "Presentación del libro 'Comunicación y poder'"
[8]: http://www.youtube.com/watch?v=HjAg4pWxW0A "Discurso de Álex de la Iglesia en los Goya 2011"
[9]: http://www.nacionred.com/legislacion-pi/el-senado-aprueba-la-ley-sinde-gracias-al-pacto-entre-pp-psoe-y-ciu "El Senado aprueba la Ley Sinde gracias al pacto entre PP, PSOE y CiU"
[10]: http://www.20minutos.es/noticia/952812/0/wikileaks/obama/leysinde/ "Obama dio en octubre de 2009 el último 'empujón' a la ley Sinde"
[11]: http://www.nolesvotes.com/ "nolesvotes.com"
[12]: https://valhue.gitlab.io/imagenes/2011/02/nolesvotes.jpg "NoLesVotes.com"

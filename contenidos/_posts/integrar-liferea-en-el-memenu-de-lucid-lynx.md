---
title: Integrar Liferea en el "MeMenu" de Lucid Lynx
date: "2010-05-09 09:15"
categories: ["Linux"]
tags: ["Informática", "Lucid", "Ubuntu"]
---

#### &nbsp; ####

Liferea es un muy buen cliente RSS que nos permite visualizar nuestros feeds sin necesidad de tener que abrir nuestro navegador y leernos por completo nuestras webs favoritas.

Quizás sea algo lento al manipular gran cantidad de feeds.

Hoy, con el MeMenu de Lucid Lynx, lo más cómodo para muchos de nosotros es tener nuestras aplicaciones cotidianas integradas a él.

Un lector de Feeds es una herramienta casi indispensable a día de hoy, y que mejor que integrarla; pues bien, ya es posible y simple.

![liferea-indicator MeMenu][1]

<!-- more -->

Primero, añadiremos el correspondiente repositorio PPA para lucid de la manera siguiente:

``` bash
    sudo add-apt-repository ppa:sikon/liferea-libindicate
```

A continuación actualizamos nuestro sistema, y después instalamos el correspondiente paquete:

``` bash
    sudo aptitude update
```

Si ya teníamos instalado Liferea:

``` bash
    sudo aptitude safe-upgrade
```

Y para quienes no tuviesen instalado Liferea:

``` bash
    sudo aptitude install liferea
```

Espero que os haya servido.

Saludos &nbsp; ** :D **

[1]: https://valhue.gitlab.io/imagenes/2010/05/liferea-indicator.png "Liferea-indicator MeMenu"

---
title: configurando raspbian en nuestra RaspBerry PI
date: 2017-04-09 16:16:57
categories: RaspBerry PI
tags:
  - RaspBeerry PI
  - Linux
  - Raspbian
---

Si estás leyendo ésta entrada, es porque ya tienes instalado [**Raspbian**](https://www.raspbian.org/) (el Sistema Operativo) en tu **RasPI** (RaspBerry PI). También puedes visitar [ésta](https://valhue.gitlab.io/entradas/2017/04/02/instalando-raspbian-en-nuestra-sd/) entrada del blog para ver cómo se instala.

#### **1. Ajustamos el sistema**

A continuación procederemos a realizar una serie de ajustes a nuestro SO. Para ello teclea desde una consola o terminal, y utilizando la línea de comandos:

``` bash
$> sudo raspi-config
```
En primer lugar expandiremos la partición donde se aloja nuestro sistema operativo. Esta partición está “***clonada***” desde la imagen que descargamos y grabamos en la tarjeta SD, pero el tamaño no tiene porque coincidir con él de nuestra SD y seguramente no lo hará. Razón por la cuál ejecutamos esta orden, para aprovechar el espacio de nuestra SD al 100%.<!-- more -->

![](https://valhue.gitlab.io/imagenes/2017/04/raspi-config.png)

{% colorquote danger %}
Todas las Raspberrys PI en un primer momento tienen el mismo usuario ( pi ) y la misma contraseña ( raspberry ).
{% endcolorquote %}

Tampoco sería una mala idea cambiar la contraseña de nuestro usuario. Si no queremos complicarnos mucho, no es necesario crear un nuevo usuario, pero creo que es evidente que no podemos dejar la contraseña que viene “de serie” con la imagen que hemos grabado.

![](https://valhue.gitlab.io/imagenes/2017/04/change-passwd.png)

![](https://valhue.gitlab.io/imagenes/2017/04/passwd-dialog.png)

Se abrirá una terminal dónde ingresaremos la nueva contraseña y al aceptar con “Enter” nos aparecerá:

![](https://valhue.gitlab.io/imagenes/2017/04/passwd-changed.png)

En la opción "**3 Boot Options**" del menú principal elegiremos cómo queremos que se inicie nuestra RasPI. Nosotros optaremos por un entorno "***no gráfico***", sin escritorio.

![](https://valhue.gitlab.io/imagenes/2017/04/boot-options.png)

 La 4ª opción, “Wait for Network at Boot”, no es necesario ajustar nada en ella. Es más, ni la tocamos. Se trata de ajustar si queremos iniciar la red antes que cualquier otro servicio de nuestro sistema. La dejamos conforme está.

Llegamos a la 5ª entrada del menú de configuración:

![](https://valhue.gitlab.io/imagenes/2017/04/locale-settings.png)

Aquí nos detendremos un poco, pues en esta opción ajustamos las “locale” y el teclado, dejando el sistema preparado y adaptado al idioma seleccionado.

![](https://valhue.gitlab.io/imagenes/2017/04/change-locale.png)

En primer lugar seleccionamos las “locales” que vamos a utilizar en nuestro sistema.

![](https://valhue.gitlab.io/imagenes/2017/04/locales.png)

Nos desplazaremos por la lista y seleccionamos con ayuda de la barra espaciadora de nuestro teclado “es_ES.UTF-8 UTF-8”, aceptamos y en una nueva pantalla nos preguntará cual de todas queremos que sea la predeterminada

![](https://valhue.gitlab.io/imagenes/2017/04/entorno.png)

Elegimos **es_ES.UTF-8** y aceptamos. Con esto nuestro sistema estará ajustado al idioma **español de España** y usará el juego de **caracteres UTF-8**. Puede que el sistema se tome un tiempo en realizar este cambio, no nos pongamos nerviosos y esperamos a que termine. Una vez se hayan realizado estos cambios regresaremos al menú principal, dónde otra vez elegiremos la 5ª entrada.

A continuación ajustaremos la zona horaria.

![](https://valhue.gitlab.io/imagenes/2017/04/timezone.png)

Seleccionamos Europa/Madrid y aceptamos.

![](https://valhue.gitlab.io/imagenes/2017/04/europe-madrid.png)

Ahora ajustaremos el teclado, queremos que todos los signos y caracteres alfanuméricos estén en su sitio.

![](https://valhue.gitlab.io/imagenes/2017/04/keyboard-layout.png)

Aquí lo dejamos tal que así:

![](https://valhue.gitlab.io/imagenes/2017/04/keyboard.png)

![](https://valhue.gitlab.io/imagenes/2017/04/keyboard-2.png)

![](https://valhue.gitlab.io/imagenes/2017/04/keyboard-3.png)

Las opciones 6 y 7 del menú de configuración nos las saltamos. No vamos a conectar ninguna cámara a nuestra RasPI por lo cual no es necesario configurar nada. Tampoco tenemos interés en añadir nuestra SoC a un mapeado internacional que realiza la fundación Raspberry Pi para contar cuantas tarjetas están funcionando y que servicios se están ejecutando en ellas.

Opción 8 del menú de configuración. Nos permite forzar la CPU de la RasPI.

![](https://valhue.gitlab.io/imagenes/2017/04/overclock.png)

No está disponible en el modelo Raspberry PI v3. En la primera edición las prestaciones no eran ni parecidas a las de esta ultima, por lo que se permitía el ***overclocking***, siempre bajo la responsabilidad del usuario, y con la debida refrigeración.

![](https://valhue.gitlab.io/imagenes/2017/04/choose-overclock.png)

Ahora realizaremos algunos cambios más “avanzados”.

![](https://valhue.gitlab.io/imagenes/2017/04/advanced-options.png)

Se trata de elegir que servicios queremos que se activen en el inicio del sistema.

![](https://valhue.gitlab.io/imagenes/2017/04/services.png)

Nosotros nos aseguraremos de que está activado el acceso SSH. A partir de ahora será el método que utilizaremos para gestionar nuestra RasPI.

![](https://valhue.gitlab.io/imagenes/2017/04/ssh.png)

{% colorquote warning %}
Podemos activar el acceso por ssh creando un archivo vacío, con el nombre ssh (sin extensión) en la 1ª partición que se crea en la SD. La RasPI al encontrarlo activa el servicio y elimina el archivo. Si no te ves capacitado para hacer esto, mejor que utilices el método anterior.
{% endcolorquote %}

También podemos instalar un **servidor rdp** para poder conectarnos y controlar la RaspPI desde el escritorio, en modo gráfico. No es nuestro caso, hemos elegido una imagen sin escritorio, pero podría servirnos para futuras instalaciones y/o nuevos proyectos.

``` bash
$> sudo apt-get install xrdp
```
#### **2. Desactivamos la wifi y el Bluetooth**

Como la RasPI v3 lleva incorporados un dispositivo NIC inalámbrico y otro de tipo bluetooth que, para nuestros propósitos, no vamos a utilizar los desactivaremos durante el inicio del Sistema Operativo (SO en adelante). De este modo reducimos el consumo de nuestro SoC. Para ello procederemos a editar el siguiente archivo del sistema:

``` bash
$> sudo nano /etc/modprobe.d/raspi-blacklist.conf
```

Y añadiremos lo siguiente:
```
#wifi
blacklist brcmfmac
blacklist brcmutil

#bt
blacklist btbcm
blacklist hci_uart
```

En el próximo reinicio ya no se cargaran estos módulos del kernel y quedarán desactivados.

{% colorquote success %}
Ya estamos listos para trabajar con nuestra RasPI en los proyectos que queramos. El Sistema Operativo está instalado y configurado, listo para operar con él.
{% endcolorquote %}

Disfrutarlo ** ;) **

---
title: "Siete, ¿el último windows?"
date: "2009-10-24T12:14:56+01:00"
categories: ["Linux"]
tags: ["Linux", "Informática", "Microsoft", "Sistemas Operativos"]
---

Me ha llegado la siguiente noticia: [***http://tinyurl.com/yk9pr9t***][2] y tras ojear el enlace, llego a la conclusión de que podríamos estar ante la ultima versión de Windows, ya que Microsoft [M$] está trabajando en serio en un nuevo Sistema Operativo basado en BSD, un unix basado en ports, ya existe una distribución totalmente libre y que les ha hecho mucho daño a los chicos de MS, me refiero a [***openBSD***][1].

El caso es que este ***nuevo*** Windows, cosa que dudo (tardaron un montón de años en lanzar el Vista, después de numerosos cambio de nombre, y ahora en casi nada de tiempo nos quieren vender que es nuevo y totalmente reescrito, jajaja...), al igual que los anteriores, adolece de una falta de control sobre los micros actuales, no soporta el escalado, ni se lleva bien con los multi-núcleos, ni soporta mas de 4 GB de Ram, etc...

Linux si lo hace en cualquiera de sus distribuciones, y esto les ha puesto las pilas a los de M$, porque como los fieles consumidores de sus Windows se enteren que están pagando un producto que no hace lo que si, otro que es gratis y libre de distribuirse, podrían perder muchos puntos.

Os recomiendo leeros la noticia completa del enlace, no tiene desperdicio.

[1]: http://www.openbsd.org/ "openBSD"
[2]: http://tinyurl.com/yk9pr9t "Noticia Original"

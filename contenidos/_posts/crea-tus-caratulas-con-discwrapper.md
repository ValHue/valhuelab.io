---
title: "Crea tus carátulas con DiscWrapper"
date: "2009-12-08T11:15:17+01:00"
categories: ["Linux"]
tags: ["Caratulas", "Linux", "Multimedia", "Ubuntu"]
---

[***DiscWrapper***][1] es un diseñador de caratulas de discos de fabricación casera. Soporta CD-DVD estándar, CD Slim, DVD, etc...

Cómo no existe un repositorio desde donde instalarlo, vamos a hacerlo compilando el programa.

Desde que los chicos de [***GetDeb***][2] han actualizado su portal, nos han dejado sin un montón de aplicaciones de las que disponíamos para Ubuntu Jaunty y anteriores. Todo esto me imagino que será temporal, y a medida que pase el tiempo irán re-compilando las aplicaciones para karmic e incluyéndolas en su renovado portal.

Pero mientras esto ocurre puedes entrar en la [***versión anterior de GetDeb***][3] y descargar software compilado hasta la versión Jaunty de Ubuntu. (Aveces se pueden mezclar programas de distintas versiones y no ocurre nada malo, pero no te lo aconsejo. ¡Si lo haces es bajo tu total
responsabilidad!).

![][6]

Instalaremos DiscWrapper siguiendo estos fáciles pasos:

1 Descargamos el paquete fuente desde [***aquí***][4].

2 Instalamos las dependencias de ejecución y las de compilación.
``` bash
    sudo aptitude install build-essential
    sudo aptitude install coreutils
    sudo aptitude install libwxbase2.8-0, libwxbase2.8-dev, libwxgtk2.8-0, libwxgtk2.8-dev, wx2.8-headers, wx2.8-i18n
```
3 Descomprimimos el paquete descargado y lo compilamos
``` bash
    tar xzvf discwrapper*tar.gz
    cd discwrapper
    ./configure
    make
    sudo make install
```
Ya tendremos agregado al menú ***Aplicaciones->Gráficos*** nuestra entrada de ***DiscWrapper***.

¡¡Que lo disfrutéis!!

[1]: http://discwrapper.sourceforge.net/ "DiscWrapper"
[2]: http://www.getdeb.net/updates/ "GetDeb"
[3]: http://old.getdeb.net/ "Versión anterior de GetDeb"
[4]: http://master.dl.sourceforge.net/sourceforge/discwrapper/discwrapper-1.2.2.tar.gz
[5]: https://valhue.gitlab.io/imagenes/2009/12/screenshot-300x236.jpg "DiscWrapper"
[6]: https://valhue.gitlab.io/imagenes/2009/12/screenshot.jpg "DiscWrapper"

---
title: "Imágenes de las carátulas de tus CDs en Nautilus"
date: "2010-02-16 16:15"
categories: ["Linux"]
tags: ["Linux", "Karmic", "Multimedia", "Ubuntu"]
---

[**Cover thumbnailer**][1] es un script escrito en Python que nos remplaza en Nautilus, el icono de una carpeta de audio con la caratula del CD. El programa detecta la presencia de un archivo de imagen y automáticamente lo sustituye como icono de la carpeta que lo contiene.

En caso de no encontrar ninguna caratula podemos elegir que nos muestre el icono por defecto o mantener el icono de nuestro tema.

También funciona para las carpetas de imágenes, creando un icono con miniaturas de algunas de las imágenes de la carpeta.

[![ScreenShot][2]][3]

<!-- more -->

Se ha actualizado a su versión 0.7 con esta lista de cambios:

- The install script now works on Fedora, Mandriva and other distributions
-  New options avalables
-  New translations (English, Danish, French, Polish, Russian and Spanish)

Para instalarlo en karmic haz lo siguiente:

``` bash
    sudo add-apt-repository ppa:flozz/flozz
    sudo apt-get update
    sudo apt-get install  cover-thumbnailer
```

Después de su instalación para activarlo haz:

``` bash
    nautilus -q && nautilus & exit
```

Por último, desde Sistema -> Preferencias -> **Cover Thumbnailer** lanzas el programa y lo configuras.

Que lo disfrutéis.

[1]: http://software.flogisoft.com/cover-thumbnailer/en/ "Cover thumbnailer"
[2]: https://valhue.gitlab.io/imagenes/2010/02/screenshot_pictures-300x215.png
[3]: https://valhue.gitlab.io/imagenes/2010/02/screenshot_pictures.png "Cover thumbnailer"


---
title: instalando raspbian en nuestra SD
date: 2017-04-02 12:27:14
categories: RaspBerry PI
tags:
  - RaspBerry PI
  - Linux
  - Raspbian
---

{% blockquote %}
Inicio con esta entrada una nueva categoría en el blog dónde iré subiendo distintos artículos orientados a ayudar a aquellas personas interesadas en trabajar con una Raspberry PI. Empezaremos con lo más básico, {% raw %}***¿cómo se instala el sistema operativo en la Raspi (Raspberry PI)?***{% endraw %}
{% endblockquote %}

#### **1. Justificación**

Nosotros elegiremos [**Raspbian Jessie Lite**](https://downloads.raspberrypi.org/raspbian_lite_latest), que es  una imagen mínima, basada en Debian Jessie. La razón es que no necesitamos entorno gráfico para iniciarnos ni vamos a utilizar ningún escritorio desde ella.

[**Raspbian**](https://www.raspbian.org/) es un sistema operativo libre basado en Debian y optimizado para el hardware de la Raspberry Pi. Un sistema operativo es el conjunto de programas básicos y utilidades que hacen funcionar tu Raspberry Pi. Sin embargo, Raspbian proporciona más que un sistema operativo puro: viene con más de 35.000 paquetes, software precompilado incluido en un formato agradable para una fácil instalación en tu Raspberry Pi.

{% colorquote info %}
Para nuestro propósito, no usaremos la Debian de escritorio o portátil, ya que estas están desarrolladas y compiladas para funcionar con procesadores x86, arquitectura que NO es compatible con el procesador de nuestra RasPI, que es un ARM.
{% endcolorquote %}

Una vez descargada la imagen y guardada en el disco duro del ordenador que estamos utilizando para crear la SD, procedemos a quemar o grabar la imagen.<!-- more-->


#### **2. Quemando la SD**

Para este fin y dependiendo de nuestro sistema operativo dispondremos de numeroso software que nos ayudará en el proceso. En el caso de linux, podemos utilizar un programa con entorno gráfico, como el caso de [**etcher**](https://etcher.io/), o bien utilizar la [**linea de comandos**](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md).

Desde un terminal, y **utilizando la linea de comandos**, escribiremos:  
```
dd bs=4M if=2017-01-11-raspbian-jessie.img of=/dev/sdc
```
Una vez terminada la ejecución del comando tendremos la imagen en nuestra SD y si la insertamos en la Raspberry PI y alimentamos a ésta, arrancará el sistema.


#### **3. Actualización**

Una vez encendida la Raspberry PI, y el Sistema Operativo haya terminado de cargar, nuestra primera tarea será la de actualizar el sistema. Linux es un  Sistema Operativo en constante desarrollo, y el hecho de que sus licencias permitan la distribución y la modificación de sus archivos y fuentes, contribuyen a que constantemente se publiquen actualizaciones.

Para actualizar nuestro sistema teclearemos desde la consola:  
``` bash
$> sudo apt-get update
$> sudo apt-get upgrade
$> sudo apt-get dist-upgrade
```
Desde Diciembre de 2014, debemos instalar también el siguiente paquete, para aplicar las mejoras en la interfaz de usuario tras actualizar la distribución; tengamos el modelo que tengamos de Raspberry, es totalmente recomendable hacerlo.
``` bash
$> sudo apt-get install raspberrypi-ui-mods
```
También podemos actualizar el firmware de nuestra SoC. Al igual que ocurre con una placa base de un PC, a nuestra Raspberry le podemos actualizar la BIOS. Para ello se procede del siguiente modo:
``` bash
$> sudo apt-get install -y ca-certificates git-core binutils
$> sudo wget https://raw.github.com/Hexxeh/rpi- update/master/rpi-update
$> sudo cp rpi-update /usr/local/bin/rpi-update
$> sudo chmod +x /usr/local/bin/rpi-update
$> sudo rpi-update
```
Una vez terminado reiniciaremos para que todos los cambios se apliquen:
``` bash
$> sudo reboot
```
{% colorquote success %}
Ya tendremos nuestra RaspBerry PI operativa y con el Sistema Operativo instalado. En próximas entregas veremos cómo configurar éste.
{% endcolorquote %}

Disfrutarlo ** ;) **
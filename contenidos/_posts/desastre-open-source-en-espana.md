---
title: "Desastre Open Source en España"
date: "2009-12-10T22:10:12+01:00"
categories: ["General"]
tags: ["Linux", "Informática"]
---

![][3]

Como señalan en [**MuyComputer**][1], el acuerdo recién alcanzado por Microsoft y el Ministerio de Educación en colaboración con las distintas Comunidades Autónomas **deja claro que la iniciativa Escuela 2.0 estará totalmente basada en productos propietarios de Microsoft**. Todo un varapalo para las alternativas Open Source que varios grupos de apoyo al Software Libre habían tratado de situar como alternativas. Microsoft ha logrado nuevamente imponer sus productos, algo que limitará la visibilidad de otras alternativas para el futuro.

La noticia completa la tienes [***aquí***][1].

<center>***¡Cruz de Pais!, siempre en contra de las tendencias.***</center>

***Justo cuando Microsoft lanza su ultima versión de Windows y [se pasa a Unix/BSD][2], le vende a nuestro querido Gobierno los restos de su "ventanucos".***

&nbsp; :( &nbsp;

[1]: http://tinyurl.com/ygpk84o "Noticia completa en MuyComputer"
[2]: http://www.barrelfish.org/ "The Barrelfish Operating System"
[3]: https://valhue.gitlab.io/banners/opensource.jpg

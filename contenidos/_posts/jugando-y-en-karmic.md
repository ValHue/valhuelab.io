---
title: "¡¡Jugando!! y en Karmic"
date: "2009-11-26T22:00:12+01:00"
categories: ["Linux"]
tags: ["Linux", "Juegos", "Karmic"]
---

![][4]

Como mucha gente, parece que con desconocimiento, de causa afirma que no se puede jugar en Linux, voy a explicar aquí la forma mas fácil y rápida de instalar algunos juegos que los chicos de [***PlayDeb***][1] nos brindan desde su web.

Yo me he instalado [***AssaultCube***][2] del cual os dejo aquí un vídeo.

<p style="text-align: center;">
    <iframe width="480" height="360" src="//www.youtube.com/embed/_S6u-pRudAM" frameborder="0" allowfullscreen></iframe>
</p>

Ahora entremos en materia, y veamos como hacerlo.

En primer lugar añadimos el repositorio de PlayDeb, el cual una vez hecho nos permitirá instalar los juegos directamente desde su web, o bien desde el gestor de paquetes Synaptic, aptitude o apt-get. Para facilitaros aun mas la labor os dejo aquí el enlace.

[***playdeb\_0.3-1~getdeb1\_all.deb***][3]

Después de esta difícil tarea ya estaremos en disposición de instalar desde la propia web el juego que más nos guste. O bien abrir Synaptic, actualizar y elegir los juegos que nos cargará desde el recién añadido repositorio.

Saludos!!

[1]: http://www.playdeb.net "PlayDeb"
[2]: http://www.playdeb.net/updates/?page=8#assaultcube "AssaultCube"
[3]: http://archive.getdeb.net/install_deb/playdeb_0.3-1~getdeb1_all.deb "Paquete instalador repo PlayDeb"
[4]: http://valhue.gitlab.io/banners/ubuntu_karmic.jpg

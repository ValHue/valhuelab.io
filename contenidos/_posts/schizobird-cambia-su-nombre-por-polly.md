---
title: "Schizobird cambia su nombre por Polly"
date: "2011-08-11T16:07:00+01:00"
categories: ["Linux"]
tags: ["Alpha", "Cliente", "Linux", "Informática", "Natty", "Polly", "SchizoBird", "Twitter", "Ubuntu"]
---

#### &nbsp; ####

Este cliente de twitter escrito en python continua su desarrollo sin descanso. Ahora además cambia de nombre, después de una encuesta que el desarrollador realizó entre sus usuarios, se ha adoptado el nombre de **"polly"**.

El proyecto cambia también su URL en launchpad, tal como se aprecia en la imagen siguiente, pero para su utilización, todavía en fase alpha y sin un paquete .deb que instalar, sigue siendo la misma que con schizobird, eso sí, ha sufrido muchas actualizaciones y correcciones a algunos bugs.

Sigo pensando que este podría ser el cliente definitivo. Ánimo y probadlo, no os va a decepcionar...

<!-- more -->[![][1]][2]

>Desde la revisión 96 se puede iniciar minimizado añadiendo la opción **--minimized** al ejecutable.
>
>Así pues, podemos copiar **polly.desktop** a **~/.config/autostart**, editarlo y donde pone **Exec=polly** lo dejamos en **Exec=polly --minimized**.
>
>En el próximo inicio de sesión polly se iniciará minimizado en nuestro indicador de mensajes

[1]: https://valhue.gitlab.io/imagenes/2011/08/Acerca-de-Polly_001.png "Acerca de Polly"
[2]: https://launchpad.net/polly "Acerca de Polly"

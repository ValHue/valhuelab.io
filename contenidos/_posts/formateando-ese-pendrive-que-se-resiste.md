---
title: "Formateando ese Pendrive que se resiste..."
date: "2011-08-28T13:15:07+01:00"
categories: ["Linux"]
tags: ["Linux", "Informática"]
---

#### &nbsp; ####

¿Nunca os ha ocurrido que un PenDrive no se deja formatear?

En Linux podemos hacerlo en unos sencillos pasos. Pero para si nos equivocamos de unidad de almacenamiento, podríamos eliminar los datos de nuestro disco duro, lo primero es identificar el dispositivo en cuestión.

Para ello, y de una forma muy simple, teclearemos en la terminal, sin el PenDrive:
``` bash
    ls /dev/sd*
```
en mi caso obtengo:

[![][1]][1]

y ahora lo mismo pero con el PenDrive conectado:<!-- more -->

[![][2]][2]

-- ***¿cual es la diferencia entre ambas imágenes?***, creo que resulta evidente.

A la vista de estos resultados podemos decir que mi PenDrive es el dispositivo **/dev/sdb** y este, a su vez, tiene una partición **/dev/sdb1**.

A continuación vamos a probar por lo más fácil
``` bash
    sudo mkfs.vfat /dev/sdb1
```
Con esto formateamos la partición 1 de nuestro PenDrive, y le damos formato vfat, que equivale al FAT32 de M$, para que nuestro PenDrive sea legible en cualquier equipo o PC.

-- ¡Eureka, lo hemos conseguido! &nbsp; :D &nbsp;

Pues hasta aquí.

----------

-- !Pues no hemos podido con él! &nbsp; :( &nbsp;

No es el fin, podemos hacerlo de otro modo más agresivo.

Vamos a cargarnos todo el contenido del PenDrive. A ver quien es el que más puede, si el cacharrito o nosotros.
``` bash
    sudo dd if=/dev/zero of=/dev/sdb
```
con esto pondremos a cero todo el dispositivo, incluso nos cargaremos la tabla de particiones.

Si vemos que se toma mucho tiempo el comando anterior, y como se trata de destruir el contenido del PenDrive, podemos hacer Control+Z con tranquilidad y retirar el dispositivo. El problema (que no lo es tanto) es que ahora nuestro sistema no detectará el PenDrive en el mismo /dev de antes, este habrá desaparecido. Bueno podemos reiniciar o buscar de nuevo el dispositivo con:
``` bash
    ls /dev/sd*
```
El siguiente paso es crear una partición en el, pero hemos destruido incluso la tabla de particiones, por lo que primero la tenemos que crear.
``` bash
    sudo fdisk /dev/sdb
```
y el propio programa fdisk nos advierte de que la tabla esta borrada y que tiene que crear una. Le obedecemos y lo hacemos.

¡¡ ***Ya tenemos el PenDrive listo para formatearlo*** !!

Si decidimos crearla con el propio fdsik volvemos a usar el comando anterior y elegimos la opción

**n**, para crear una nueva partición

**p**, para que esta sea primaria

y le asignamos un numero del 1 al 4 (**os recuerdo que un dispositivo de almacenamiento solo puede tener 4 particiones primarias**),

**w**, para guardar los cambios y salir de fdisk

Hemos creado manualmente la partición, solo falta darle formato, y para eso procedemos como en el primer supuesto
``` bash
    sudo mkfs.vfat /dev/sdb1
```
Espero que esta entrada os sea de utilidad

Un saludo.

[1]: https://valhue.gitlab.io/imagenes/2011/08/Selección_0011.png "Imagen"
[2]: https://valhue.gitlab.io/imagenes/2011/08/Selección_001.png "Imagen"

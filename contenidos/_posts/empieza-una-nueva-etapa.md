---
title: "Empieza una nueva etapa"
date: "2010-09-02T10:23:45+01:00"
categories: ["El Blog"]
tags: ["El Blog", "Web"]
---

Como podéis ver he cambiado la dirección del blog, esto implica que he registrado el dominio valhue.es y ahora está alojado en un servidor privado (de pago).

Los de Orgfree.com no han contestado a ninguno de mis correos pidiendo una explicación de porque me suspenden la cuenta vhuelamo.orgfree.com, pues no he subido ningún script malicioso, todo el contenido es mio o es GNU, y que narices, que no me han avisado de nada y lo han cerrado sin dar explicaciones. ***Me han borrado todo el contenido***, y lo gracioso es que la cuenta sigue activa, por lo menos puedo entrar por FTP, pero allí no hay nada y no quiero que esto vuelva a suceder.

En fin, quienes me leíais  ya sabéis que ahora [**http://vhuelamo.orgfree.com**][1] pasa a estar en [**http://blog.valhue.es**][2]

Saludos a tod@s. &nbsp; :D &nbsp;

[1]: http://vhuelamo.orgfree.com "URL antigua"
[2]: http://blog.valhue.es "URL Actual"

---
title: "Instalar AbiWord 2.8.1 desde un repositorio"
date: "2009-12-01T19:41:23+01:00"
categories: ["Linux"]
tags: ["Linux", "Informática", "Karmic", "Oficina"]
---

![][4]

Desde finales de Octubre existe la versión 2.8.x de este procesador de textos. Mucho más ligero que OpenOffice y para mí la mejor alternativa a Microsoft Office. La existencia de esta versión la descubrí [***aquí***][1], uno de los blogs que sigo para estar al día en lo que a Ubuntu se refiere.

Entre las mejoras de esta nueva versión destacan:

-  Soporte de **anotaciones y comentarios** en los textos.
-  Soporte para **"citas inteligentes"** que permiten entrecomillar partes del texto con comillas rectas o curvas según el tipo de cita.
-  Vista **multipágina**
-  Soporte para ***Scalable Vector Graphics (SVG)***
-  Mejor soporte para **ficheros ODT y DOCX**

Hasta ahora para instalar la versión linux teníamos que descargar el archivo fuente desde la web de AbiWord, pero he encontrado [***este repositorio ppa***][2], que nos simplifica mucho el proceso:

``` bash
    sudo add-apt-repository ppa:guido-iodice/abiword-2.8
    sudo aptitude update
    sudo aptitude install abiword
```

Ya he probado la principal novedad, que consiste en la posibilidad de colaborar y compartir los documentos mediante [***AbiCollab***][3], y es una maravilla. **AbiCollab es un servicio similar a Google Docs o a Zoho Writer**: nos permitirá disponer de cierto espacio en los servidores de AbiWord para alojar en ese espacio nuestros documentos de texto, de forma que podamos trabajar con ellos también a través de Internet.

A esa importante característica se le une que en AbiWord 2.8 podremos colaborar **para escribir un documento entre varias personas en tiempo real**. Esto de debe a la tecnología Telepathy -que es la base del cliente de mensajería instantánea Empathy, ahora por defecto en Ubuntu 9.10- los cambios y modificaciones aparecen a medida que los distintos colaboradores los realizan.

¡Que lo disfrutéis!

Saludos.

[1]: http://elsoftwarelibre.wordpress.com/2009/10/31/abiword-2-8-0-con-importantisimas-novedades "AbiWord 2.8.0"
[2]: https://launchpad.net/~guido-iodice/+archive/abiword-2.8 "Repositorio PPA"
[3]: http://www.abicollab.net/ "AbiCollab"
[4]: https://valhue.gitlab.io/banners/ubuntu_karmic.jpg

---
title: "Schizobird, cliente ¿definitivo? de Twitter"
date: "2011-07-26T01:45:08+01:00"
categories: ["Linux"]
tags: ["Clientes", "Linux", "Informática", "Natty", "Python", "Twitter", "Ubuntu"]
---

#### &nbsp; ####

Schizobird es un cliente GTK+ de Twitter especialmente diseñado para varias columnas y/o varias cuentas.

Esta aplicación es todavía pre-alpha y no tiene paquetes disponibles todavía. Sin embargo, se puede probar fácilmente la rama actual (bzr branch lp: schizobird) mediante la ejecución del ejecutable "schizobird" de el directorio raíz (*NO* el de la carpeta "bin").

<!-- more -->[![][1]][2]

Schizobird requiere Python 2.7 (disponible en Ubuntu desde 11,04) y por defecto usa el menú de mensajería. Los usuarios que no tienen el menú de mensajería, o simplemente no lo deseen utilizar se pueden cambiar a un icono de notificación de la zona tradicional con el parámetro "*--statusicon*".

Todas las bibliotecas siguientes son necesarias, pero las únicas que no se instalan por defecto en Natty son las tres primeras. La última sólo es necesaria si se utiliza el menú de mensajería.

``` bash
    * python-socksipy
    * python-oauth2
    * python-numpy


    * python-gtk2
    * python-dbus
    * python-xdg
    * python-notify
    * python-gconf
    * python-httplib2
    * python-pycurl
    * python-keyring
    * python-gtkspell


    * python-indicate
```

Los usuarios de Ubuntu Natty haremos lo siguiente. Abrimos una terminal e instalaremos las tres primeras librerias con...

``` bash
    sudo apt-get install python-socksipy python-oauth2 python-numpy
```

nos vamos al directorio donde guardemos las librerías del usuario, en mi caso ~/.local/lib

``` bash
    cd ~/.local/lib
```

una vez en ese directorio ejecutaremos

``` bash
    bzr branch lp:schizobird
```

ahora crearemos unos cuantos enlaces para que todo quede en su sitio

``` bash
    ln -s ~/.local/lib/schizobird/schizobird ~/.local/bin/
    ln -s ~/.local/lib/schizobird/icons/* ~/.local/share/icons/
    ln -s ~/.local/lib/schizobird/share/applications/* ~/.local/share/applications/
    ln -s ~/.local/lib/schizobird/share/schizobird/ ~/.local/share/
```

a continuación creamos la entrada en el menú de gnome

``` bash
    gedit ~/.local/share/applications/schizobird.desktop
```

y le asignamos correctamente el icono para la entrada del menú cambiando

``` bash
    Icon=schizobird
```

por

``` bash
    Icon=schizobird-48
```

además tenemos que agregar

``` bash
    Categories=GTK;GNOME;Network;
```

para que nos aparezca en la categoría de internet del menú.

Esto es todo amigos, es un muy buen cliente, y eso que está en fase alpha de desarrollo, esperemos que pronto nos brinden un PPA para instalarlo con más facilidad y que añadan más servicios además de Twitter, (el autor ya comenta de añadirle Identica, Buzz, etc...)

Un saludo y que lo disfruteis

** :D **

[1]: https://valhue.gitlab.io/imagenes/2011/07/large-270x300.png "Schizobird"
[2]: https://valhue.gitlab.io/imagenes/2011/07/large.png "Schizobird"

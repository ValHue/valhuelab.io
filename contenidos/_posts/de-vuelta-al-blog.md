---
title: "De vuelta... al Blog."
date: "2012-11-26T17:04:12+01:00"
categories: ["El Blog"]
tags: ["El Blog", "Web"]
---

Como podréis comprobar hacía más de un año que no escribía una entrada,
ni "medio" comentario. Estaba ocupado en otros menesteres y para que
ocultarlo un tanto decepcionado con algunas personas, con algunos hechos
y /o acontecimientos que han ocurrido en mi circulo personal.

En fin, espero que a partir de ahora empiece un nuevo periodo y no falte
tanto tiempo sin dejar constancia de mis evoluciones en este apasionante
mundo del GNU/Linux.

Un saludo.

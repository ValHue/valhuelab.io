---
title: "Polly ya tiene PPA"
date: "2011-08-15T10:20:06+01:00"
categories: ["Linux"]
tags: ["Alpha", "Cliente", "Linux", "Informática", "Natty", "Polly", "SchizoBird", "Twitter", "Ubuntu"]
---

#### &nbsp; ####

Desde que empezaron las primeras versiones alpha, y se podía probar el entonces llamado schizobird, vengo avisando de este cliente twitter. Pues bien, aun en versión alpha, pero dada su espectacular evolución y el interés despertado, ya se dispone de un PPA para instalar Polly.

Abriremos una terminal y haremos lo siguiente:
``` bash
    sudo add-apt-repository ppa:conscioususer/polly-daily
    sudo apt-get update
    sudo apt-get install polly
```

***Si hubiésemos tenido una instalación manual desde bzr, tal como indico en mis otras entradas, antes de ejecutar los comandos anteriores procederemos a eliminarla.***

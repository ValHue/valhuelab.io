function addSmiley() {
    var postBodyClass = (postBodyClass) ? postBodyClass : 'article-entry';
    var d = document.getElementsByClassName(postBodyClass);
    for (var i = 0; i < d.length; i++) {
        d[i].innerHTML = d[i].innerHTML.replace(/ \:\) /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/00.gif" title="Sonrisa" alt="Sonrisa" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:S /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/01.gif" title="Desconcertado" alt="Desconcertado" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:P /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/02.gif" title="Bromeando" alt="Bromeando" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:\[ /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/03.gif" title="Lloroso" alt="LLoroso" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:D /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/04.gif" title="Riendo" alt="Riendo" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \}\) /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/05.gif" title="Diabólico" alt="Diabólico" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ X\* /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/06.gif" title="Ooops!" alt="Ooops!" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \;D /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/07.gif" title="Carcajada" alt="Carcajada" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:\| /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/08.gif" title="Serio" alt="Serio" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:\} /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/09.gif" title="Burla" alt="Burla" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:\*\) /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/10.gif" title="Sonrojado" alt="Sonrojado" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \:\( /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/11.gif" title="Triste" alt="Triste" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ \;\) /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/12.gif" title="Guiño" alt="Guiño" /> ');
        d[i].innerHTML = d[i].innerHTML.replace(/ XD /gi, ' <img id="smile" src="https://valhue.gitlab.io/smile/13.gif" title="Risa Irónica" alt="Risa Irónica" /> ')
    }
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func
    } else {
        window.onload = function() {
            oldonload();
            func()
        }
    }
}
addLoadEvent(addSmiley);

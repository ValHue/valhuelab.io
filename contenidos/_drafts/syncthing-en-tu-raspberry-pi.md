---
title: Syncthing en tu Raspberry PI
date: 2019-03-03 19:38:01
categories: ["Raspberry PI"]
tags: ["Linux", "Raspberry PI", "Syncthing"]
---

Esta entrada tratará de como tener un servicio de almacenamiento propio en la nube. Para guardar de manera segura la información de tus dispositivos, existe la opción de utilizar un servicio externo como podría ser DropBox, o en este caso instalar en tu Raspberry PI un servicio que te permite guardar todos tus archivos importantes y acceder a ellos desde cualquier sitio, manteniendo sincronizado su contenido con el dispositivo que tu quieras.

Eso es lo que vamos a hacer, y de una manera muy sencilla. Para ese cometido instalaremos Syncthing, pero:

### ***- ¿que es Syncthing?***

Syncthing es una aplicación multiplataforma, que permite la sincronización entre dispositivos utilizando la tecnología P2P, ya sea en una red local o en Internet. Syncthing reemplaza los servicios propietarios (es software libre) de sincronización y nube por un sistema abierto, confiable y descentralizado. **Tus datos son solo tus datos** y mereces elegir dónde guardarlo, si lo quieres compartir con un tercero y cómo enviarlo por Internet.



### **INSTALACIÓN:**




window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#edeff5",
      "text": "#838391"
    },
    "button": {
      "background": "#3273dc"
    }
  },
  "theme": "classic",
  "content": {
    "message": "Utilizamos cookies de terceros para mejorar nuestros servicios y garantizar que obtenga la mejor experiencia.",
    "dismiss": "Acepto",
    "link": "Leer m&aacute;s",
    "href": "https://europa.eu/european-union/abouteuropa/cookies_es"
  }
})});
